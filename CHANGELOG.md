# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## [Unreleased]

### Added

- test: add support debian 12

### Removed

- test: remove support debian 10

### Changed

- test: use personal docker registry

## [v1.0.0] - 2020-08-09

- first version
