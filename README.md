# Ansible role: Gitlab Runner

[![Version](https://img.shields.io/badge/latest_version-1.0.0-green.svg)](https://code.waks.be/nishiki/ansible-role-gitlab/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-gitlab/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-gitlab_runner/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-gitlab_runner/actions?workflow=molecule.yml)

Install and configure a Gitlab runner

## Requirements

- Ansible >= 2.9
- Debian
  - Bullseye
  - Bookworm

## Role variables

- `gitlab_runner_runners` - array with the runners to manage

```
  - name: docker on machine
    executor: docker
    url: https://gitlab.example.com
    token: Secret
    options:
      docker-image: 'alpine:latest'
      locked: false
```

- `gitlab_runner_env_variables` - dict with environment variables

```
  HTTP_PROXY: https://127.0.0.1:3218
```

## How to use

```
- hosts: server
  roles:
    - gitlab_runner
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule 'molecule[docker]' docker ansible-lint testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2020 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
