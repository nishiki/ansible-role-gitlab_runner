import testinfra.utils.ansible_runner

def test_packages(host):
  package = host.package('gitlab-runner')
  assert package.is_installed

def test_env_file(host):
  path = host.file('/etc/systemd/system/gitlab-runner.service.d/ansible.conf')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644
  assert path.contains('Environment="TEST=toto"')

def test_config_file(host):
  config = host.file('/etc/gitlab-runner/config.toml')
  assert config.exists
  assert config.is_file
  assert config.user == 'root'
  assert config.group == 'root'
  assert config.mode == 0o600

def test_service(host):
  service = host.service('gitlab-runner')
  assert service.is_running
  assert service.is_enabled
